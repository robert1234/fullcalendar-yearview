export class MonthDayNumberConstructor {
    construct(year: number) {
        let result = {};
        for (let i = 0; i < 12; i++) {
            result[i] = this.getDaysInMonth(i, year);
        }
    }

    getDaysInMonth(month,year) {
        return new Date(year, month+1, 0).getDate();
    }
}