import { createPlugin } from '@fullcalendar/common'
import { DayTableView } from './DayTableView'
import { TableDateProfileGenerator } from './TableDateProfileGenerator'
import { OPTION_REFINERS } from './options'
import './options-declare'
import './main.css'

export * from './api-type-deps'
export { DayTable, DayTableSlicer } from './DayTable'
export { Table } from './Table'
export { TableSeg } from './TableSeg'
export { TableCellModel } from './TableCell'
export { TableView } from './TableView'
export { buildDayTableModel } from './DayTableView'

export default createPlugin({
  initialView: 'dayGridMonth',
  optionRefiners: OPTION_REFINERS,
  views: {

    dayGridYear: {
      component: DayTableView,
      dateProfileGeneratorClass: TableDateProfileGenerator
    },

  }
})
