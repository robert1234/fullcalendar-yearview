import {MonthDayNumberConstructor} from "./MonthDayNumber";

export class CalendarGridGenerator {
    private year: number;
    private monthDayNumberGenerator = new MonthDayNumberConstructor();

    constructor(year: number) {
        this.year = year;
    }

    getMonthDayNumber() {
        this.monthDayNumberGenerator.construct(this.year);
    }
}